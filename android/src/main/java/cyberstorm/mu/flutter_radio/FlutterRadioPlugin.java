package cyberstorm.mu.flutter_radio;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.Objects;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * FlutterRadioPlugin
 */
public class FlutterRadioPlugin implements MethodCallHandler {

    private static EventChannel.EventSink mStateEventSink;
    private static EventChannel.EventSink mStationNameEventSink;
    private static EventChannel.EventSink mMetadataEventSink;
    private static EventChannel.EventSink mCloseAppEventSink;
    private static EventChannel.EventSink mStartedAppEventSink;
    private static EventChannel.EventSink mTimeRemainingEventSink;
    private static EventChannel.EventSink mTotalTimeEventSink;
    // To get the application Context
    private Context context;
    //Service Binder
    private LocalService mService;
    //To check if we have been bound
    private boolean mBound = false;
    private String playerState = C.PLAYBACK_STATE_IDLE;

    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {

            mService = ((LocalService.LocalBinder) service).getService();

//            Toast.makeText(context, "Service Connected ", Toast.LENGTH_SHORT).show();

            mBound = true;
            if (  mStartedAppEventSink != null) {
                mStartedAppEventSink.success(true);
            }

            MediaPlayer radio = mService.initializeRadio();

            radio.setOnEventListener(new MediaPlayerData() {
                @Override
                public void updateStationMetadata(String metadata) {
                    Log.d("RADIOPLAYER STREAM 1", metadata);
                    if (mMetadataEventSink != null) {
                        mMetadataEventSink.success(Utils.getTitle(metadata) + "|" + Utils.getArtists(metadata));
                    }
                }

                @Override
                public void updateStationName(String radioName) {
                    Log.d("RADIOPLAYER STREAM 2", radioName);
                    if (mStationNameEventSink != null) {
                        mStationNameEventSink.success(Utils.getRadio(radioName));
                    }
                }

                @Override
                public void updateRadioState(String state) {

                    if (mStateEventSink != null) {
                        Log.d("RADIOPLAYER STREAM 3", state);
                        playerState = state;
                        mStateEventSink.success(state);
                    }
                }

                @Override
                public void updateTimeRemaining(Long milliseconds) {
                    if (mTimeRemainingEventSink != null) {
                        Log.d("RADIOPLAYER STREAM 4", String.valueOf(milliseconds));
                        mTimeRemainingEventSink.success(milliseconds);
                    }
                }

                @Override
                public void updateTotalTime(Long milliseconds) {
                    if (mTotalTimeEventSink != null) {
                        Log.d("RADIOPLAYER STREAM 5", String.valueOf(milliseconds));
                        mTotalTimeEventSink.success(milliseconds);
                    }
                }

            });

            mService.setOnEventListener(new StopListener() {
                @Override
                public void closeApp() {
                    mCloseAppEventSink.success(true);

                    Intent intent = new Intent(context, LocalService.class);
                    context.stopService(intent);
                    context.unbindService(connection);
                    mBound = false;
                }
            });


        }

        public void onServiceDisconnected(ComponentName className) {
            mBound = false;
        }
    };

    public FlutterRadioPlugin(Context context) {
        this.context = context;
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        FlutterRadioPlugin plugin = new FlutterRadioPlugin(registrar.context());
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_radio");
        channel.setMethodCallHandler(plugin);

        initializeStreams(registrar.messenger());

    }

    private static void initializeStreams(BinaryMessenger messenger) {

        new EventChannel(messenger, C.STREAM_STATE).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mStateEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_RADIO_NAME).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mStationNameEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_METADATA).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mMetadataEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_STOP).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mCloseAppEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_IS_STARTED).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mStartedAppEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_TIME_REMAINING).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mTimeRemainingEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

        new EventChannel(messenger, C.STREAM_TOTAL_TIME).setStreamHandler(new EventChannel.StreamHandler() {
            @Override
            public void onListen(Object o, EventChannel.EventSink eventSink) {
                mTotalTimeEventSink = eventSink;
            }

            @Override
            public void onCancel(Object o) {

            }
        });

    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {

        if (call.method.equals("initialiseRadio")) {
            if (!mBound && playerState.equals(C.PLAYBACK_STATE_IDLE)) {
                Log.d("STATION INIT", "INIT");
                Thread t = new Thread() {
                    public void run() {
                        Intent intent = new Intent(context, LocalService.class);
                        context.bindService(intent, connection, Context.BIND_AUTO_CREATE);
                    }
                };
                mBound = true;
                t.start();
            }

            result.success(true);
        } else if (mBound) {
            Log.d("STATION BOUND", "In bound");
            switch (call.method) {
                case C.PLAYBACK_ACTION_PLAY:

                    mService.play();

                    result.success(true);
                    break;
                case C.PLAYBACK_ACTION_PAUSE:

                    mService.pause();

                    result.success(true);
                    break;
                case C.PLAYBACK_ACTION_NEXT:

                    mService.playNextStation();

                    result.success(true);
                    break;
                case C.PLAYBACK_ACTION_PREVIOUS:

                    mService.playPreviousStation();

                    result.success(true);
                    break;

                case C.PLAYBACK_ACTION_STOP:

                    mService.stop();

                    result.success(true);
                    break;
                case C.PLAYBACK_ACTION_GET_INDEX:

                    result.success(mService.getStationIndex());
                    break;
                case C.PLAYBACK_ACTION_TO_INDEX:
                    String index = call.argument("index");

                    mService.playStationIndex(Integer.parseInt(index));

                    result.success(mService.getStationIndex());
                    break;
                case C.PLAYBACK_ACTION_SET_STATIONS:
//                    Toast.makeText(this.context, "STATION BEGIN", Toast.LENGTH_LONG).show();

                    ArrayList<String> stations = call.argument("stations");
                    Log.d("STATION BEGIN", stations.get(0));
                    mService.setStations(stations);

                    result.success(true);
                    break;
                case C.PLAYBACK_ACTION_SEEK_TO:


                    Long milliseconds = Long.parseLong(String.valueOf(Objects.requireNonNull(call.argument("milliseconds"))));
                    mService.seekTo(milliseconds);

                    result.success(true);
                    break;
                default:
                    result.notImplemented();
                    break;
            }
        } else {
            result.notImplemented();
        }
        Log.d("STATION BOUND", "Done");
    }

}
