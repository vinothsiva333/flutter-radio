package cyberstorm.mu.flutter_radio;

import java.util.List;

public interface RadioPlayerInterface {
    void playNextStation();
    void play();
    void pause();
    void stop();
    void playStationIndex(int index);
    void setStations(List<String> stations);
    void destroyPlayer();
    int getStationIndex();
    void playPreviousStation();
    void seekTo(Long playbackPosition);
}
