package cyberstorm.mu.flutter_radio;

public interface MediaPlayerData {
    void updateStationMetadata(String metadata);
    void updateStationName(String radioName);
    void updateRadioState(String state);
    void updateTimeRemaining(Long milliseconds);
    void updateTotalTime(Long milliseconds);


}
