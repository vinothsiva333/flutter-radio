package cyberstorm.mu.flutter_radio;

public interface StopListener {
    void closeApp();
}
