package cyberstorm.mu.flutter_radio;

public class C {
    public static final String PLAYBACK_CHANNEL_ID = "flutter_radio";
    public static final String PLAYBACK_CHANNEL_NAME = "RADIO PLAYER";
    public static final String PLAYBACK_CHANNEL_DESCRIPTION = "RADIO NOTIFICATION";
    public static final int PLAYBACK_NOTIFICATION_ID = 101;

    public static final String PLAYBACK_STATE_PLAY = "media.player.playing";
    public static final String PLAYBACK_STATE_PAUSE = "media.player.paused";
    public static final String PLAYBACK_STATE_IDLE = "media.player.idle";
    public static final String PLAYBACK_STATE_STOP = "media.player.stopped";
    public static final String PLAYBACK_STATE_LOADING = "media.player.loading";
    public static final String PLAYBACK_STATE_ERROR = "media.player.error";


    public static final String PLAYBACK_ACTION_PLAY = "media.action.play";
    public static final String PLAYBACK_ACTION_PAUSE = "media.action.pause";
    public static final String PLAYBACK_ACTION_STOP = "media.action.stop";
    public static final String PLAYBACK_ACTION_NEXT = "media.action.next";
    public static final String PLAYBACK_ACTION_PREVIOUS = "media.action.previous";
    public static final String PLAYBACK_ACTION_TO_INDEX = "media.action.toIndex";
    public static final String PLAYBACK_ACTION_GET_INDEX = "media.action.getIndex";
    public static final String PLAYBACK_ACTION_SET_STATIONS = "media.action.setPlaylist";
    public static final String PLAYBACK_ACTION_SEEK_TO = "media.action.seekTo";

    public static final String STREAM_METADATA = "mu.cyberstorm.eventchannelmetadata/stream";
    public static final String STREAM_STATE = "mu.cyberstorm.eventchannelstate/stream";
    public static final String STREAM_RADIO_NAME = "mu.cyberstorm.eventchannelradio/stream";
    public static final String STREAM_INDEX = "mu.cyberstorm.eventchannelindex/stream";
    public static final String STREAM_STOP = "mu.cyberstorm.eventchannelstop/stream";
    public static final String STREAM_IS_STARTED = "mu.cyberstorm.eventchannelisStarted/stream";
    public static final String STREAM_TIME_REMAINING = "mu.cyberstorm.eventchanneltimeRemaining/stream";
    public static final String STREAM_TOTAL_TIME = "mu.cyberstorm.eventchanneltotalTime/stream";
}
