package cyberstorm.mu.flutter_radio;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class MediaPlayer implements RadioPlayerInterface, MetadataOutput, TextOutput, Player.EventListener {


    private boolean autoPlay = false;
    private String currentState = C.PLAYBACK_STATE_IDLE;
    private MediaPlayerData mediaPlayerDataCallback = null;
    private MediaPlayerData flutterInterfaceCallback = null;


    private SimpleExoPlayer player;
    private DefaultDataSourceFactory dataSourceFactory;
    private ConcatenatingMediaSource concatenatingMediaSource;
    private Context context;
    private MediaSource mediaSource;
    private String radioData = "";
    private String radioMetadata = "";
    Long playbackPosition = 0L;
    private int currentWindow = 0;
    private Handler mHandler;

    MediaPlayer() {}

    MediaPlayer(boolean autoPlay, Context context) {
        this.autoPlay = autoPlay;
        this.context = context;
        this.player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
        this.dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "flutter_radio"));
        this.registerEvent(new NotificationPlayerManager(context, "", "", ""));

    }

    public void registerEvent(MediaPlayerData callback) {
        this.mediaPlayerDataCallback = callback;

    }

    public void setOnEventListener(MediaPlayerData callback) {
        this.flutterInterfaceCallback = callback;
        Log.d("RADIOPLAYER CALLBACK", "INITIALIZED");
    }

    @Override
    public void playNextStation() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER STATE", String.valueOf(this.getPlayer().getCurrentWindowIndex()));
            this.getPlayer().next();
        }
    }

    @Override
    public void play() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER STATE", "PLAY");
            this.getPlayer().setPlayWhenReady(true);
        }
    }

    @Override
    public void pause() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER STATE", "PAUSE");
            this.getPlayer().setPlayWhenReady(false);
        }
    }

    @Override
    public void stop() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER STATE", "STOP");
            this.getPlayer().stop();
        }
    }

    @Override
    public void playStationIndex(int index) {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER INDEX", String.valueOf(index));
            this.getPlayer().seekToDefaultPosition(index);
        }
    }


    @Override
    public void setStations(List<String> stations) {
        this.concatenatingMediaSource = new ConcatenatingMediaSource();


        for (String station : stations) {
            this.mediaSource = new ExtractorMediaSource.Factory(this.dataSourceFactory)
                    .createMediaSource(Uri.parse(station));
            this.concatenatingMediaSource.addMediaSource(this.mediaSource);
        }

        this.player.addMetadataOutput(this);
        this.player.addTextOutput(this);
        this.player.addListener(this);
        this.player.prepare(this.concatenatingMediaSource);
        this.player.setForegroundMode(true);
        this.player.setPlayWhenReady(this.autoPlay);
        mHandler = new Handler();
        mHandler.post(updateProgressAction);
    }

    @Override
    public void destroyPlayer() {
        player.release();
        player = null;
    }

    @Override
    public int getStationIndex() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER INDEX", String.valueOf(this.getPlayer().getCurrentWindowIndex()));
            return this.getPlayer().getCurrentWindowIndex();
        }
        return -1;
    }

    @Override
    public void playPreviousStation() {
        if (this.getPlayer() != null) {
            Log.d("RADIOPLAYER INDEX", String.valueOf(this.getPlayer().getCurrentWindowIndex()));
            this.getPlayer().previous();
        }
    }

    @Override
    public void seekTo(Long playbackPosition) {
//        playbackPosition = this.player.getCurrentPosition();
        currentWindow = this.player.getCurrentWindowIndex();
        this.player.seekTo(currentWindow, playbackPosition);
    }

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private void updateProgress() {
        Long remaining = this.player.getDuration() - this.player.getCurrentPosition();
        this.flutterInterfaceCallback.updateTimeRemaining(remaining);
        this.flutterInterfaceCallback.updateTotalTime(this.player.getDuration());
        Log.d("TIME", String.valueOf(remaining));
        long delayMs = TimeUnit.SECONDS.toMillis(1);
        mHandler.postDelayed(updateProgressAction, delayMs);
    }

    @Override
    public void onMetadata(Metadata metadata) {
        Log.d("RADIOPLAYER METADATA", metadata.get(0).toString());
        this.mediaPlayerDataCallback.updateStationMetadata(metadata.get(0).toString());
        if(this.flutterInterfaceCallback != null) {
            this.flutterInterfaceCallback.updateStationMetadata(metadata.get(0).toString());
        }
        this.radioMetadata = metadata.get(0).toString();
    }

    @Override
    public void onCues(List<Cue> cues) {

    }

    public boolean isAutoPlay() {
        return autoPlay;
    }

    public void setAutoPlay(boolean autoPlay) {
        this.autoPlay = autoPlay;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ExoPlayer getPlayer() {
        return this.player;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        for (int i = 0; i < trackGroups.length; i++) {
            TrackGroup trackGroup = trackGroups.get(i);
            for (int j = 0; j < trackGroup.length; j++) {
                Metadata trackMetadata = trackGroup.getFormat(j).metadata;
                if (trackMetadata != null) {
                    Log.d("RADIOPLAYER NAME", trackMetadata.get(0).toString());

                    this.mediaPlayerDataCallback.updateStationName(trackMetadata.get(0).toString());
                    if(this.flutterInterfaceCallback != null) {
                        this.flutterInterfaceCallback.updateStationName(trackMetadata.get(0).toString());
                    }
                    this.radioData = trackMetadata.get(0).toString();
                }
            }
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                this.currentState = C.PLAYBACK_STATE_LOADING;

                break;
            case ExoPlayer.STATE_ENDED:
                this.currentState = C.PLAYBACK_STATE_STOP;

                break;
            case ExoPlayer.STATE_IDLE:
                this.currentState = C.PLAYBACK_STATE_IDLE;
                break;
            case ExoPlayer.STATE_READY:
                if (playWhenReady) {
                    this.currentState = C.PLAYBACK_STATE_PLAY;
                } else {
                    this.currentState = C.PLAYBACK_STATE_PAUSE;
                }
                break;
            default:
                Log.d("RADIOPLAYER STATE", C.PLAYBACK_STATE_ERROR);
                break;
        }
        Log.d("RADIOPLAYER STATE", this.currentState);
        this.mediaPlayerDataCallback.updateRadioState(this.currentState);
        if(this.flutterInterfaceCallback != null) {
            this.flutterInterfaceCallback.updateRadioState(this.currentState);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        this.currentState = C.PLAYBACK_STATE_ERROR;
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getRadioMetadata() {
        return radioMetadata;
    }

    public String getRadioData() {
        return radioData;
    }
}
