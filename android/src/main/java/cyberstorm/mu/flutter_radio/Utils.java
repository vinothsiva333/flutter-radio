package cyberstorm.mu.flutter_radio;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static String getArtists(String text) {
        Pattern pattern = Pattern.compile("\"(.*?)-");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
        {
            return matcher.group(1).trim();
        }
        return "Dreamsky Radio";
    }

    public static String getTitle(String text) {
        Pattern pattern = Pattern.compile("-(.*?)\",");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
        {
            return matcher.group(1).trim();
        }
        return "Fresh hits";
    }

    public static String getRadio(String text) {
        Pattern pattern = Pattern.compile("\"(.*?)\"");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find())
        {
            return matcher.group(1).trim();
        }
        return "Dreamsky Radio";
    }
}
