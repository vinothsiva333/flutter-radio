package cyberstorm.mu.flutter_radio;

public class MusicServiceHolder {

    private static boolean serviceStarted;
    private static  String radioName = "";
    private static  String title  = "";
    private static  String artists  = "";
    private static  String state  = "";
    public String color;

    public boolean isServiceStarted() {
        return serviceStarted;
    }

    public void setServiceStarted(boolean serviceStarted) {
        MusicServiceHolder.serviceStarted = serviceStarted;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        MusicServiceHolder.state = state;
    }

    public String getRadioName() {
        return radioName;
    }

    public void setRadioName(String radioName) {
        MusicServiceHolder.radioName = radioName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        MusicServiceHolder.title = title;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        MusicServiceHolder.artists = artists;
    }

}
