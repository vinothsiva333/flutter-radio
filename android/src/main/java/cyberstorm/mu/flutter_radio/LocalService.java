package cyberstorm.mu.flutter_radio;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LocalService extends Service implements RadioPlayerInterface {
    private final IBinder binder = new LocalBinder();
    private StopListener flutterCallback = null;

    private MediaPlayer radioPlayer = new MediaPlayer();
    private NotificationPlayerManager notificationPlayerManager = new NotificationPlayerManager();

    private List<String> stations = new ArrayList<>();

    private boolean isFirst;
    private boolean isLast;

    @Override
    public IBinder onBind(Intent intent) {
//        Toast.makeText(this, "Service OnBind()", Toast.LENGTH_LONG).show();
        return binder;
    }

    public void setOnEventListener(StopListener callback) {
        this.flutterCallback = callback;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        final Context context = this;
//        Toast.makeText(this, "Service Created", Toast.LENGTH_SHORT).show();
        this.notificationPlayerManager = new NotificationPlayerManager(this, "", "", "");
        startForeground(C.PLAYBACK_NOTIFICATION_ID, this.notificationPlayerManager.buildNotificationPlayer());
        this.radioPlayer = new MediaPlayer(true, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent == null || intent.getAction() == null)
            return START_STICKY;

        String action = intent.getAction();
        this.isLast = this.radioPlayer.getStationIndex() == (this.stations.size() - 1);
        this.isFirst = this.radioPlayer.getStationIndex() == 0;

        boolean isEmpty =  this.stations.size() == 0;

        switch (action) {
            case C.PLAYBACK_ACTION_NEXT:
                if (this.isLast || isEmpty) {
                    Toast.makeText(this, "Fin de la playlist", Toast.LENGTH_SHORT).show();
                } else
                    this.radioPlayer.playNextStation();
                break;
            case C.PLAYBACK_ACTION_PLAY:
                if (isEmpty) {
                    Toast.makeText(this, "Playlist vide...", Toast.LENGTH_SHORT).show();
                } else this.radioPlayer.play();
                break;
            case C.PLAYBACK_ACTION_PAUSE:
                if (isEmpty) {
                    Toast.makeText(this, "Playliste vide...", Toast.LENGTH_SHORT).show();
                } else this.radioPlayer.pause();
                break;
            case C.PLAYBACK_ACTION_PREVIOUS:

                if (this.isFirst) {
                    Toast.makeText(this, "Fin de la playlist", Toast.LENGTH_SHORT).show();
                } else
                    this.radioPlayer.playPreviousStation();
                break;
            case C.PLAYBACK_ACTION_STOP:
                this.stop();
                stopSelf();
            default:
                break;
        }

        Log.d("RADIOPLAYER STATE", this.radioPlayer.getCurrentState());
        return START_STICKY;
    }

    public MediaPlayer initializeRadio() {
        return this.radioPlayer;
    }


    @Override
    public void onDestroy() {
//        Toast.makeText(this, "Service Destroyed ", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void playNextStation() {
        this.radioPlayer.playNextStation();
    }

    @Override
    public void play() {
        this.radioPlayer.play();
    }

    @Override
    public void pause() {
        this.radioPlayer.pause();
    }

    @Override
    public void stop() {
        this.stopForeground(false);
        this.radioPlayer.stop();
        this.flutterCallback.closeApp();
        this.radioPlayer.destroyPlayer();
        this.notificationPlayerManager.cancelNotification();
    }

    @Override
    public void playStationIndex(int index) {
        this.radioPlayer.playStationIndex(index);
    }

    @Override
    public void setStations(List<String> stations) {

        this.stations = stations;
        this.radioPlayer.setStations(stations);
//        Toast.makeText(this, "STATION SET", Toast.LENGTH_LONG).show();
        Log.d("STATION", stations.get(0).toString());
    }

    @Override
    public void destroyPlayer() {
        this.radioPlayer.destroyPlayer();
    }

    @Override
    public int getStationIndex() {
       return this.radioPlayer.getStationIndex();
    }

    @Override
    public void playPreviousStation() {
        this.radioPlayer.playPreviousStation();
    }

    @Override
    public void seekTo(Long playbackPosition) {
        this.radioPlayer.seekTo(playbackPosition);
    }


    public class LocalBinder extends Binder {
        LocalService getService() {
            return LocalService.this;
        }
    }

}
