package cyberstorm.mu.flutter_radio;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

public class NotificationPlayerManager implements MediaPlayerData {

    private Context context;
    private String radioName;
    private String title;
    private String artists;
    private Bitmap artwork;
    MusicServiceHolder musicServiceHolder = new MusicServiceHolder();
    private NotificationCompat.Builder notificationPlayer;
    NotificationCompat.Style style;




    NotificationPlayerManager() {
    }

    NotificationPlayerManager(Context context, String radioName, String title, String artists) {
        this.context = context;
        createNotificationChannel();
        updateNotification();
        buildNotificationPlayer();
    }

    private NotificationCompat.Action generateAction(int icon, String title, String intentAction) {

        Intent intent = new Intent(this.getContext(), LocalService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(this.getContext(), 1, intent, 0);
        return new NotificationCompat.Action.Builder(icon, title, pendingIntent).build();
    }

    public void updateNotification() {

        this.artwork = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.dreamsky);
        this.notificationPlayer = new NotificationCompat.Builder(this.context, C.PLAYBACK_CHANNEL_ID)
                .setSmallIcon(R.drawable.dreamsky_icon)
                .setOnlyAlertOnce(true)
                .setSubText(musicServiceHolder.getRadioName())
                .setColor(0x03A9F4)
                .setShowWhen(false)
                .setLargeIcon(this.artwork)
                .setContentTitle(musicServiceHolder.getTitle())
                .setContentText(musicServiceHolder.getArtists());

        if (this.context != null ) {
            Intent launchIntent = this.getContext().getPackageManager().getLaunchIntentForPackage("mu.dreamskyradio.app");

            if (launchIntent != null) {
                PendingIntent pIntent = PendingIntent.getActivity(this.getContext(), 0, launchIntent, 0);
                this.notificationPlayer = this.notificationPlayer
                        .setContentIntent(pIntent);
            }


            Intent intent = new Intent(this.getContext(), LocalService.class);
            intent.setAction(C.STREAM_STOP);
            PendingIntent pendingIntent = PendingIntent.getService(this.getContext(), 1, intent, 0);

            this.notificationPlayer = this.notificationPlayer
                    .setDeleteIntent(pendingIntent);
        }

        style = new androidx.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0,1,2);

        this.notificationPlayer = this.notificationPlayer
                .addAction(generateAction(R.drawable.exo_icon_previous, "Previous", C.PLAYBACK_ACTION_PREVIOUS));

        if ( ! musicServiceHolder.getState().equals(C.PLAYBACK_STATE_PLAY)) {

            this.notificationPlayer = this.notificationPlayer
                    .addAction(generateAction(R.drawable.exo_icon_play, "Play", C.PLAYBACK_ACTION_PLAY));
        }else {
            this.notificationPlayer = this.notificationPlayer
                    .addAction(generateAction(R.drawable.exo_icon_pause, "Pause", C.PLAYBACK_ACTION_PAUSE));
        }

        this.notificationPlayer = this.notificationPlayer
                .addAction(generateAction(R.drawable.exo_icon_next, "Next", C.PLAYBACK_ACTION_NEXT));
        this.notificationPlayer = this.notificationPlayer
                .addAction(generateAction(R.drawable.ic_dialog_close_dark, "Stop", C.PLAYBACK_ACTION_STOP));

        this.notificationPlayer = this.notificationPlayer
                .setStyle(style)
                .setPriority(NotificationCompat.PRIORITY_MAX);
        Log.d("RADIOPLAYER NOTIFY",  musicServiceHolder.getArtists());
        Context app = this.getContext().getApplicationContext();

        this.buildNotificationPlayer();
    }

    public void  createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(C.PLAYBACK_CHANNEL_ID, C.PLAYBACK_CHANNEL_NAME, importance);
            channel.setDescription(C.PLAYBACK_CHANNEL_DESCRIPTION);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = this.getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

    }

    @Override
    public void updateStationMetadata(String metadata) {
        musicServiceHolder.setTitle( Utils.getTitle(metadata));
        musicServiceHolder.setArtists( Utils.getArtists(metadata));
        Log.d("RADIOPLAYER EVENT",  musicServiceHolder.getTitle());
        Log.d("RADIOPLAYER EVENT",  musicServiceHolder.getArtists());
        this.updateNotification();
    }

    @Override
    public void updateStationName(String radioName) {
        musicServiceHolder.setRadioName(Utils.getRadio(radioName));
        Log.d("RADIOPLAYER EVENT",  musicServiceHolder.getRadioName());
        this. updateNotification();
    }

    @Override
    public void updateRadioState(String state) {
        Log.d("RADIOPLAYER EVENT", state);
        musicServiceHolder.setState(state);
        if (state.equals(C.PLAYBACK_ACTION_STOP)) {
            this.notificationPlayer.setAutoCancel(true)
                    .build().flags |= Notification.FLAG_AUTO_CANCEL;
            this.cancelNotification();
        }
        this. updateNotification();
    }

    @Override
    public void updateTimeRemaining(Long milliseconds) {

    }

    @Override
    public void updateTotalTime(Long milliseconds) {

    }


    public Notification buildNotificationPlayer() {

        Context app = this.getContext().getApplicationContext();
        NotificationManager nm = (NotificationManager) app
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(C.PLAYBACK_NOTIFICATION_ID, notificationPlayer.build());

        return notificationPlayer.build();
    }

    public void cancelNotification() {
        Context app = this.getContext().getApplicationContext();
        NotificationManager nm = (NotificationManager) app
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(C.PLAYBACK_NOTIFICATION_ID);
    }

    // getters and setters

    public NotificationCompat.Builder getNotificationPlayer() {
        return notificationPlayer;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getRadioName() {
        return radioName;
    }

    public void setRadioName(String radioName) {
        this.radioName = radioName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public Bitmap getArtwork() {
        return artwork;
    }

    public void setArtwork(Bitmap artwork) {
        this.artwork = artwork;
    }

}
