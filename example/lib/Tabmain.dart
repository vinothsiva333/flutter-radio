import 'package:flutter/material.dart';
import 'calls.dart';
import 'music.dart';
import 'package:bmnav/bmnav.dart' as bmnav;

void main() {
	runApp(MaterialApp(
		home: TabHome(),
		theme: ThemeData(primaryColor: Colors.green),
	));
}

class TabHome extends StatefulWidget {
	@override
	MainWidgetState createState() => MainWidgetState();
}

class MainWidgetState extends State<TabHome> {
	int currentTab = 0;
	final List<Widget> screens = [
		new MusicApp(),
          new Calls(),
          new Calls(),
 
	];
	Widget currentScreen = MusicApp();
	
	final PageStorageBucket bucket = PageStorageBucket();
	
	@override
	Widget build(BuildContext ctx) {
		return Scaffold(
			backgroundColor: Colors.black,
			body: PageStorage(child: currentScreen, bucket: bucket),
				bottomNavigationBar: bmnav.BottomNav(
				index: currentTab,
				iconStyle: bmnav.IconStyle(onSelectColor: Colors.white,size: 30.0,onSelectSize: 30.0),
				
				color: Colors.black,
				labelStyle: bmnav.LabelStyle(visible: true,onSelectTextStyle: TextStyle(color: Colors.white)),
				
				onTap: (i) {
					setState(() {
						currentTab = i;
						currentScreen = screens[i];
					});
				},
				items: [
					
					bmnav.BottomNavItem(
						Icons.home, label: 'Home'
					),
					bmnav.BottomNavItem(Icons.book, label: 'Book'),
					bmnav.BottomNavItem(Icons.queue_music, label: 'Music'),
//					bmnav.BottomNavItem(Icons.favorite, label: 'Favourite'),
//					bmnav.BottomNavItem(Icons.account_circle, label: 'Account')
				],
			),
			
		);
	}
}








//
//void main() async {
//	runApp(new MaterialApp(
//		debugShowCheckedModeBanner: false,
//		home: TabHome(),
//	));
//}
//
//class TabHome extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: '',
//      debugShowCheckedModeBanner: false,
//      theme: new ThemeData(
//        primaryColor: const Color(0xFF333333),
//        primaryColorDark: const Color(0xFF333333),
//      ),
//      home: new DashboardScreen(title: ''),
//    );
//  }
//}
//
//class DashboardScreen extends StatefulWidget {
//  DashboardScreen({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _DashboardScreenState createState() => new _DashboardScreenState();
//}
//
//class _DashboardScreenState extends State<DashboardScreen> {
//
//  PageController _pageController;
//  int _page = 0;
//  String checkplay;
//
//  Future<Null> plaud() async{
//	  SharedPreferences preferences = await SharedPreferences.getInstance();
//
//	  setState(() {
//		  checkplay = preferences.getString('checkaudioplayer');
//
//	  });
//  }
//
//  @override
//  void initState() {
//    super.initState();
//    _pageController = new PageController();
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//    _pageController.dispose();
//  }
//
//  void navigationTapped(int page) {
//    // Animating to the page.
//    // You can use whatever duration and curve you like
//    _pageController.animateToPage(page,
//        duration: const Duration(milliseconds: 1), curve: Curves.ease);
//  }
//
//  void onPageChanged(int page) {
//    setState(() {
//      this._page = page;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      body: new PageView(
//		  physics: NeverScrollableScrollPhysics(),
//        children: [
//          new Cartmainpage(),
//          new Searchcart(),
//          new Artist(),
//          new Stories("Wishlist"),
//          new LoginPage(),
//        ],
//        onPageChanged: onPageChanged,
//        controller: _pageController,
//      ),
//      bottomNavigationBar: new Theme(
//        data: Theme.of(context).copyWith(
//          // sets the background color of the `BottomNavigationBar`
//          canvasColor: Color(0xFF000000),
//        ), // sets the inactive color of the `BottomNavigationBar`
//        child: new BottomNavigationBar(
////          type: BottomNavigationBarType.shifting,
//			fixedColor: const Color(0xFF333333),
//			items: [
//				new BottomNavigationBarItem(
//					icon: new Icon(
//						Icons.home,color:Colors.grey,size: 30.0,
//					),
//					activeIcon:new Icon(
//						Icons.home,color:Colors.white,size: 30.0,
//					),
//					title: new Text(
//						"",
//					),
//				),
//				new BottomNavigationBarItem(
//					icon: new Icon(
//						Icons.shopping_cart,color:Colors.grey,size: 30.0,
//					),
//					activeIcon:new Icon(
//						Icons.shopping_cart,color:Colors.white,size: 30.0,
//					),
//					title: new Text(
//						"",
//					)),
//				new BottomNavigationBarItem(
//					icon: new Icon(
//						Icons.queue_music,color:Colors.grey,size: 30.0,
//					),
//					activeIcon:new Icon(
//						Icons.queue_music,color:Colors.white,size: 30.0,
//					),
//					title: new Text(
//						"",
//					)),
//				new BottomNavigationBarItem(
//					icon: new Stack(children: <Widget>[
//						new Icon(Icons.favorite,color:Colors.grey,size: 30.0,),
//
//
//						new Positioned(
//							top: -1.0,
//							right: -1.0,
//							child: new Stack(
//								children: <Widget>[
//									new Icon(
//										Icons.brightness_1,
//										size: 12.0,
//										color: Colors.redAccent,
////							  color: const Color(0xFF2845E7),
//
//									),
//								],
//							))
//					]),
//					activeIcon: new Stack(children: <Widget>[
//						new Icon(Icons.favorite,color:Colors.white,size: 30.0,),
//
//						new Positioned(
//							top: -1.0,
//							right: -1.0,
//							child: new Stack(
//								children: <Widget>[
//									new Icon(
//										Icons.brightness_1,
//										size: 12.0,
//										color: Colors.redAccent,
//		//							  color: const Color(0xFF2845E7),
//
//									),
//								],
//							))
//					]),
//
//
//
//					title: new Text(
//						"",
//					)),
//				new BottomNavigationBarItem(
//					icon: new Icon(
//						Icons.account_circle,color:Colors.grey,size: 30.0,
//					),
//					activeIcon:new Icon(
//						Icons.account_circle,color:Colors.white,size: 30.0,
//					),
//					title: new Text(
//						"",
//					)),
//			],
//          onTap: navigationTapped,
//          currentIndex: _page,
//        ),
//      ),
//    );
//  }
//}
