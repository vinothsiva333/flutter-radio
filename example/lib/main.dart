import 'package:flutter/material.dart';
import 'music.dart';

import 'Tabmain.dart';


void main() async {
	
	runApp(new MaterialApp(
		debugShowCheckedModeBanner: false,
		home: MyApp(),
	));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
		debugShowCheckedModeBanner: false,
      title: 'Music',
      theme: new ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: new TabHome(),
    );
  }
}