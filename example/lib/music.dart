import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:seekbar/seekbar.dart';

//void main() => runApp(MyApp());

class MusicApp extends StatefulWidget {
  @override
  _MusicAppState createState() => _MusicAppState();
}

class _MusicAppState extends State<MusicApp> with WidgetsBindingObserver {
  bool _isRunning = false;
  List<String> stations = [
    "http://www.largesound.com/ashborytour/sound/brobob.mp3", // 30 sec
    "http://www.largesound.com/ashborytour/sound/AshboryBYU.mp3", // 1:15 mins
    "http://151.80.44.127:8060/stream",
    "http://151.80.44.127:8248/stream",
//    "http://51.15.208.163:8081/radio/MirchiTopTucker/icecast.audio"
  ];

  static int twoDigitMilliseconds;
  static int twoDigitseconds;
  static String RemainingTime;
  static String totalTime;

  double _value = 0.0;
  int totalTimeFromStream = 0;
  double _secondValue = 0.0;
//  double _value = (double.tryParse(RemainingTime)/double.tryParse(totalTime) ) * 100;
  _updateProgress(data) {
    print("VALUE PROGRESS $data");
    setState(() {
      totalTimeFromStream = data;
      _secondValue = 1;
    });
  }

  _updateRemaining(data) {
    print("VALUE PROGRESS REMAINING $data");

    setState(() {
      _value = 1 - ( data   / totalTimeFromStream) ;
    });
    print(_value);
  }



  Timer _progressTimer;
  Timer _secondProgressTimer;

  bool _done = false;

  @override
  void initState() {
    super.initState();
    listeners();
    initPlatformState();

  }

  @override
  void dispose() {
    _progressTimer?.cancel();
    _secondProgressTimer?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
    if (state == AppLifecycleState.suspending) {
      FlutterRadio.stop;
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    bool isRunning;
    try {
      isRunning = await FlutterRadio.initialiseRadio;
    } on PlatformException {
      _isRunning = false;
    }
    print("RUNNING");

    if (!mounted) return;

    setState(() {
      _isRunning = isRunning;
    });
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    twoDigitMilliseconds = duration.inMinutes;
    twoDigitseconds = duration.inSeconds;

    // it it will do HH:mm:ss, you can turn it to mm:ss , UP TO YOU
    return "$twoDigitMinutes:$twoDigitSeconds";
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://thinkcontemporary.com/wp-content/uploads/2018/11/Digital-Hub-10-800.jpg'),
                      fit: BoxFit.cover)),
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Text('Running on: $_isRunning\n',
                      style: TextStyle(color: Colors.white)),
                ),
                StreamBuilder(
                  stream: FlutterRadio.radioState,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    if (snapshot.hasData) {
                      return Text('${snapshot.data}',
                          style: TextStyle(color: Colors.white));
                    }
                    return Text(
                      'Wait',
                      style: TextStyle(color: Colors.white),
                    );
                  },
                ),
                StreamBuilder(
                  stream: FlutterRadio.metadata,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    if (snapshot.hasData) {
                      return Text('${snapshot.data}',
                          style: TextStyle(color: Colors.white));
                    }
                    return Text('Wait', style: TextStyle(color: Colors.white));
                  },
                ),
                StreamBuilder(
                  stream: FlutterRadio.radioName,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    if (snapshot.hasData) {
                      return Text('${snapshot.data}',
                          style: TextStyle(color: Colors.white));
                    }
                    return Text('Wait', style: TextStyle(color: Colors.white));
                  },
                ),
//                StreamBuilder(
//                  stream: FlutterRadio.timeRemaining,
//                  builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//                    if (snapshot.hasData) {
//                      RemainingTime = '${_printDuration(Duration(milliseconds: snapshot.data))}';
//                      return Text(
//                          '${_printDuration(Duration(milliseconds: snapshot.data))}',
//                          style: TextStyle(color: Colors.white));
//                    }
//                    return Text('Wait', style: TextStyle(color: Colors.white));
//                  },
//                ),
//                StreamBuilder(
//                  stream: FlutterRadio.totalTime,
//                  builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//                    if (snapshot.hasData) {
//                      totalTime =
//                          '${_printDuration(Duration(milliseconds: snapshot.data))}';
//                      return Text(
//                          '${_printDuration(Duration(milliseconds: snapshot.data))}',
//                          style: TextStyle(color: Colors.white));
//                    }
//                    return Text('Wait', style: TextStyle(color: Colors.white));
//                  },
//                ),
                Expanded(
                  flex: 10,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      SizedBox(width: 50.0),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 30.0),
                  child: Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        alignment: Alignment.center,
                        color: Colors.transparent,
                        child: SeekBar(
                          value: _value,
                          secondValue: _secondValue,
                          progressColor: Colors.blue,
                          secondProgressColor: Colors.blue.withOpacity(0.5),
                          onStartTrackingTouch: () {
                            print('onStartTrackingTouch');
                            if (!_done) {
                              _progressTimer?.cancel();
                            }
                          },
                          onProgressChanged: (value) {
                            print('onProgressChanged:$value');
                            setState(() {
                              _value = value;
                            });
                          },
                          onStopTrackingTouch: () {
                            print('onStopTrackingTouch');
                            if (!_done) {
//                              _resumeProgressTimer();
                            print(_value);
                            print((_value * totalTimeFromStream).toInt().toString());
                              FlutterRadio.seekTo((_value * totalTimeFromStream).toInt());
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                Icons.playlist_play,
                                size: 42.0,
                                color: Colors.white,
                              ),
                              onPressed: () async {
//                                _resumeProgressTimer();
                                await FlutterRadio.setPlaylist(stations);
//                                _resumeProgressTimer();
                              }),
                          IconButton(
                              icon: Icon(
                                Icons.fast_rewind,
                                color: Colors.white,
                                size: 42.0,
                              ),
                              onPressed: () async {
                                await FlutterRadio.previous;
                              }),
                          IconButton(
                              icon: Icon(
                                Icons.play_arrow,
                                size: 42.0,
                                color: Colors.white,
                              ),
                              onPressed: () async {
//                                _resumeProgressTimer();
                                await FlutterRadio.play;
                              }),
                          IconButton(
                              icon: Icon(
                                Icons.pause,
                                color: Colors.white,
                                size: 42.0,
                              ),
                              onPressed: () async {
                                await FlutterRadio.pause;
                                _progressTimer?.cancel();
                                _secondProgressTimer?.cancel();
                              }),
                          IconButton(
                              icon: Icon(
                                Icons.fast_forward,
                                color: Colors.white,
                                size: 42.0,
                              ),
                              onPressed: () async {
                                await FlutterRadio.next;
                              }),
                        ],
                      ),
                    ],
                  )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void listeners() {
    print("INIT LISTENENERS");
    FlutterRadio.timeRemaining.listen(_updateRemaining);
    FlutterRadio.totalTime.listen(_updateProgress);
  }
}
