class C {
  static const String statePlay = "media.player.playing";
  static const String statePause = "media.player.paused";
  static const String stateIdle = "media.player.idle";
  static const String stateStop = "media.player.stopped";
  static const String stateLoad = "media.player.loading";
  static const String stateError = "media.player.error";

  static const String actionPlay = "media.action.play";
  static const String actionPause = "media.action.pause";
  static const String actionStop = "media.action.stop";
  static const String actionNext = "media.action.next";
  static const String actionPrevious = "media.action.previous";
  static const String actionToIndex = "media.action.toIndex";
  static const String actionGetStation = "media.action.getIndex";
  static const String actionSetPlaylist = "media.action.setPlaylist";
  static const String actionSeekTo = "media.action.seekTo";

  static const String streamMetadata = "mu.cyberstorm.eventchannelmetadata/stream";
  static const String streamState = "mu.cyberstorm.eventchannelstate/stream";
  static const String streamRadioName = "mu.cyberstorm.eventchannelradio/stream";
  static const String streamIndex = "mu.cyberstorm.eventchannelindex/stream";
  static const String streamStop = "mu.cyberstorm.eventchannelstop/stream";
  static const String streamHasStarted = "mu.cyberstorm.eventchannelisStarted/stream";
  static const String streamTimeRemaining = "mu.cyberstorm.eventchanneltimeRemaining/stream";
  static const String streamTotalTime = "mu.cyberstorm.eventchanneltotalTime/stream";

}