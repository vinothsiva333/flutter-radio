import 'dart:async';

import 'package:flutter/services.dart';
import 'C.dart';
class FlutterRadio {
  static const MethodChannel _channel =
  const MethodChannel('flutter_radio');

  static String state = C.stateIdle;
  static String currentMeta;
  static String currentRadio;
  static int currentIndex = 0;

  static EventChannel _stateEventChannel = const EventChannel(C.streamState);
  static EventChannel _metadataEventChannel = const EventChannel(C.streamMetadata);
  static EventChannel _radioNameEventChannel = const EventChannel(C.streamRadioName);
  static EventChannel _stopEventChannel = const EventChannel(C.streamStop);
  static EventChannel _hasStartedEventChannel = const EventChannel(C.streamHasStarted);
  static EventChannel _timeRemainingEventChannel = const EventChannel(C.streamTimeRemaining);
  static EventChannel _totalTimeEventChannel = const EventChannel(C.streamTotalTime);

  static Future<bool> get initialiseRadio async {

    final bool initialiseRadio = await _channel.invokeMethod('initialiseRadio');
    return initialiseRadio;
  }

  static Future<bool> get play async {
    final bool actionPlay = await _channel.invokeMethod(C.actionPlay);
    return actionPlay;
  }
  static Future<bool> get pause async {
    final bool actionPause = await _channel.invokeMethod(C.actionPause);
    return actionPause;
  }
  static Future<bool> get stop async {
    final bool actionStop = await _channel.invokeMethod(C.actionStop);
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return actionStop;
  }
  static Future<bool> get next async {
    final bool actionNext = await _channel.invokeMethod(C.actionNext);
    return actionNext;
  }
  static Future<bool> get previous async {
    final bool actionPrevious = await _channel.invokeMethod(C.actionPrevious);
    return actionPrevious;
  }
  static Future<int> setRadioIndex(int index) async {
    final int setToNext = await _channel.invokeMethod(C.actionToIndex, {"index": index.toString()});
    return setToNext;
  }
  static Future<int> get radioIndex async {
    final int indexRadio = await _channel.invokeMethod(C.actionGetStation);
    return indexRadio;
  }

  static Future<bool> setPlaylist(List<String> playlist) async {
    final bool state = await _channel.invokeMethod(C.actionSetPlaylist,{"stations": playlist});
    return state;
  }

  static Future<bool> seekTo(int milliseconds) async {
    final bool state = await _channel.invokeMethod(C.actionSeekTo,{"milliseconds": milliseconds});
    return state;
  }

  static Stream<String> get radioState {
    return  _stateEventChannel.receiveBroadcastStream().map<String>((value) => value);
  }
  static Stream<String> get metadata {
    return  _metadataEventChannel.receiveBroadcastStream().map<String>((value) => value);
  }
  static Stream<String> get radioName {
    return  _radioNameEventChannel.receiveBroadcastStream().map<String>((value) => value);
  }
  static Stream<bool> get stopRadio {
    return  _stopEventChannel.receiveBroadcastStream().map<bool>((value) => value);
  }
  static Stream<bool> get isStarted {
    return  _hasStartedEventChannel.receiveBroadcastStream().map<bool>((value) => value);
  }
  static Stream<int> get timeRemaining {
    return  _timeRemainingEventChannel.receiveBroadcastStream().map<int>((value) => value);
  }
  static Stream<int> get totalTime {
    return  _totalTimeEventChannel.receiveBroadcastStream().map<int>((value) => value);
  }
}
